package tn.esprit.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan(basePackages={"tn.esprit.controller", "tn.esprit.presentation", "tn.esprit.service", "tn.esprit.aspect"})
@EnableAspectJAutoProxy  //(proxyTargetClass=true) //pour forcer CGLIB
public class AnnotationConfig {

}
