/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.aspect;

import java.time.Instant;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component //Sinon l'aspect ne sera pas détecté
@Aspect
public class Performance {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Around("execution(* tn.esprit.controller.*.get*(..))")
	public String responseTime(ProceedingJoinPoint joinPoint) throws Throwable{
        Long startTime = Instant.now().toEpochMilli();
		String fullName = (String) joinPoint.proceed();
        Long endTime = Instant.now().toEpochMilli();
        Long elapsedTime = endTime - startTime;
        logger.info("Response time in milli seconds of : "  + joinPoint.getSignature().getName() + " is : " +elapsedTime);
        return fullName;
	}
}

