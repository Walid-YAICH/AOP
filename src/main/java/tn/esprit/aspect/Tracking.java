/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

//@Component //Sinon l'aspect ne sera pas détecté
@Aspect
public class Tracking {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Before("execution(* tn.esprit.service.ClientInfoSOAPServiceImpl.*(..))")
	public void trackSOAPCalls(JoinPoint joinPoint){
		logger.info("This SOAP Service " + joinPoint.getSignature().getName() + " is called !");
	}
}

